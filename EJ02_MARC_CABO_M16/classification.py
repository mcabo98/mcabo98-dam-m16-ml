
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns

# Búsquem i printem el dataset
iris = pd.read_csv('iris.csv')
print('\n',iris.head())

X = iris.loc[:, ['sepal_length','petal_length']]
y = iris['species']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle="true")

# K-NEAREST NEIGHBORS
knn = KNeighborsClassifier().fit(X_train, y_train)
print('\nK-NEAREST NEIGHBORS\nTrain',knn.score(X_train, y_train))
print('Test',knn.score(X_test, y_test))

# K-NEARES NEIGHBORS PLOT


# GAUSSIAN PROGRESS PLOT


# SCATTERPLOT MATRIX
sns.set_theme(style="ticks")
df = sns.load_dataset("iris")
sns.pairplot(df,hue="species")
plt.show()
