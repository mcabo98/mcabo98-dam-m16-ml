
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

fruits = pd.read_table('fruit_data_with_colors.txt')
print('\n',fruits.head())

print('\nFRUTAS TOTALES, DISTINTOS TIPOS')
print(fruits.shape)

print('\nDISTINTAS PIEZAS DE FRUTA')
print(fruits['fruit_name'].unique())

features_names = ['mass', 'width', 'height', 'color_score']
X = fruits[features_names]
y = fruits['fruit_label']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

scaler = MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

logreg = LogisticRegression()
logreg.fit(X_train, y_train)

# LOGISTIC REGRESSION
print('\nLOGISTIC REGRESSION\nTrain',logreg.score(X_train, y_train))
print('Test',logreg.score(X_test, y_test))

# DECISION TREE
clf = DecisionTreeClassifier().fit(X_train, y_train)
print('\nDECISION TREE\nTrain',clf.score(X_train, y_train))
print('Test',clf.score(X_test, y_test))

# K-NEAREST NEIGHBORS
knn = KNeighborsClassifier().fit(X_train, y_train)
print('\nK-NEAREST NEIGHBORS\nTrain',knn.score(X_train, y_train))
print('Test',knn.score(X_test, y_test))














