#
#
#
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import re
from sklearn import linear_model
from sklearn.model_selection import train_test_split

# DEF EXTRACT_RAM
def extract_ram(x):
    match = re.search(r"(\d+)GB", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la RAM", x)

# DEF EXTRACT_CPU
def extract_cpu(x):
    match = re.search(r"((\d+(\.\d+)?))GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la CPU", x)

laptops = pd.read_csv("laptops.csv", encoding="latin-1")
laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)


X = laptops.loc[:, ["Ram", "Cpu"]]
y = laptops["Price_euros"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2020, shuffle="true")

# Inicialitzem el model
regression_model = linear_model.LinearRegression()

# Entrenem el model
regression_model.fit(X_train,y_train)
print("Train",regression_model.score(X_train, y_train))
print("Test",regression_model.score(X_test, y_test))

laptops.plot(kind="scatter",
             x = ["Ram", "Cpu"],
             y = "Price_euros",
             figsize =(9,9),
             color = "blue",
             xlim = (0,7))
plt.plot(laptops["Ram", "Cpu"],
         regression_model.score(X_train, y_train),
         color="blue")

plt.show()
